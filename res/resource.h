#ifndef NOCPUID
#include <cpuid.h>
#endif /// of NOCPUID

#define APP_VERSION					0,1,0,1
#define APP_VERSION_STR				"0.1.0.1"

#define RES_FILEDESC				"Vandlion A3 configurator"

#define IDC_ICON_A                  101

