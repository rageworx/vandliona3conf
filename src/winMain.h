#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Check_Button.H>

#include <pthread.h>

#include <vector>
#include <string>

class WMain
{
    public:
        WMain( int argc, char** argv, const char* loc );
        ~WMain();

    public:
        int Run();

    public:
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }

    private:
        void createComponents();
        bool loadtimetxt();
        void defaultdata( unsigned lvl = 0 );
        void filldata();
        void apply2ui();
        bool writetimetxt();

    private:
        int         _argc;
        char**      _argv;

    private:
        bool        timermutex;
        bool        busyflag;

    protected:
        Fl_Double_Window*   window;
        Fl_Input*           inpDateYear;
        Fl_Input*           inpDateMonth;
        Fl_Input*           inpDateDay;
        Fl_Input*           inpTimeHour;
        Fl_Input*           inpTimeMinute;
        Fl_Check_Button*    chkShowDateTime;
        Fl_Choice*          chsResolution;
        Fl_Check_Button*    chkLoopRec;
        Fl_Check_Button*    chkMotion;
        Fl_Button*          btnApplyToday;
        Fl_Button*          btnSave;
        Fl_Box*             boxCopyR;

    protected:
        HICON               hIconWindowLarge;
        HICON               hIconWindowSmall;
        HICON               hIconNotifier;
        
    protected:
        int                 data_date_year;
        int                 data_date_month;
        int                 data_date_day;
        int                 data_time_hour;
        int                 data_time_minute;
        int                 data_time_second;
        unsigned            data_motion_v;
        unsigned            data_timedisp_v;
        unsigned            data_res_v;
        unsigned            data_loop_v;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];
};

#endif // __WINMAIN_H__
