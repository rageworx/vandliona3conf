// Project designed for MinGW-W64.
#include <windows.h>
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <string>
#include <iostream>
#include <fstream>

#include "winMain.h"

#include <FL/fl_draw.H>
#include <FL/fl_ask.H>

#include "resource.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "Vandlion A3 Configurator"
#define DEF_WIDGET_FSZ          12

#ifdef __linux__
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif // __linux__

#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xFFAA8800

#define APPLY_THEME_INP( _x_ )  _x_->box( FL_THIN_UP_BOX );\
                                _x_->color( fl_darker( window->color() ) );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->textcolor( 0xFF663300 );\
                                _x_->textsize( window->labelsize() );\
                                _x_->textfont( FL_COURIER );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() );\
                                _x_->callback( WMain::WidgetCB, this )
#define APPLY_THEME_BTN( _x_ )  _x_->color( window->color(), \
                                       fl_darker( window->color() ) );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() );\
                                _x_->callback( WMain::WidgetCB, this );\
                                _x_->clear_visible_focus()
#define APPLY_THEME_BOX( _x_ )  _x_->color( window->color() );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() )
#define WRITESTATUS( _x_ )      laststatus = _x_;\
                                boxLastStatus->label( laststatus.c_str() );\
                                if ( grpMain->active_r() > 0 ) \
                                    boxLastStatus->redraw()
#define APPLY_THEME_CHOICE( _x_ )   _x_->box( FL_NO_BOX );\
                                    _x_->color( window->color() );\
                                    _x_->labelcolor( window->labelcolor() );\
                                    _x_->labelsize( window->labelsize() );\
                                    _x_->textcolor( 0xFF663300 );\
                                    _x_->textsize( window->labelsize() );\
                                    _x_->textfont( window->labelfont() );\
                                    _x_->when( FL_WHEN_CHANGED );\
                                    _x_->callback( WMain::WidgetCB, this );\
                                    _x_->clear_visible_focus()

#define TIMETXT_FN              "time.txt"

////////////////////////////////////////////////////////////////////////////////

const char* _ftoa( float f )
{
    static char fstr[80] = {0};
    sprintf( fstr, "%.2f", f );
    return fstr;
}

const char* _wc2utf8( const wchar_t* src )
{
    static char retstr[512] = {0};

    memset( retstr, 0, 512 );

    fl_utf8fromwc( retstr, 512, src, wcslen( src ) );

    return retstr;
}

const wchar_t* _utf8_2wc( const char* src )
{
    static wchar_t retstr[512] = {0};

    memset( retstr, 0, 512 * sizeof(wchar_t) );

    fl_utf8towc( src, strlen( src ), retstr, 512 );

    return retstr;
}

vector<string> split(const string& s, char seperator)
{
    vector<string> output;
    string::size_type prev_pos = 0;
    string::size_type pos = 0;

    while( ( pos = s.find(seperator, pos)) != string::npos )
    {
        string substring( s.substr(prev_pos, pos-prev_pos) );

        output.push_back(substring);

        prev_pos = ++pos;
    }

    output.push_back( s.substr( prev_pos, pos-prev_pos ) ); // Last word

    return output;
}

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv, const char* loc )
 :  _argc( argc ),
    _argv( argv ),
    busyflag( false )
{
    snprintf( wintitlestr, MAX_WINTITLE_LEN, "%s v%s",
              RES_FILEDESC, APP_VERSION_STR );
              
    createComponents();

    // fl_display no long defined in fl.H since FLTK 1.4
    extern HINSTANCE fl_display;

    // load Windows Title Icons --
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    if ( window != NULL )
    {
       window->icons( hIconWindowLarge, hIconWindowSmall );
    }
}

WMain::~WMain()
{
    if ( window != NULL )
    {
        window->clear();
    }
}

int WMain::Run()
{
    if ( window != NULL )
    {
        if ( loadtimetxt() == false )
        {
            defaultdata();
        }
        
        filldata();
        apply2ui();
        
        return Fl::run();
    }

    return 0;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        window->hide();
        return;
    }
    
    if ( w == btnApplyToday )
    {
        btnApplyToday->deactivate();
        defaultdata( 1 );
        filldata();
        apply2ui();
        btnApplyToday->activate();
        return;
    }
    
    if ( w == btnSave )
    {
        btnSave->deactivate();
        apply2ui();
        if ( writetimetxt() == true )
        {
            fl_message_title( "Success" );
            fl_message( "Data written to file successfully." );
        }
        else
        {
            fl_message_title( "Failure" );
            fl_alert( "Failed to write file, check write protection or permission." );
        }
        btnSave->activate();
        return;
    }
}

void WMain::createComponents()
{
    unsigned ww = 500;
    unsigned wh = 120;

    window = new Fl_Double_Window( ww, wh, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        fl_message_size_ = DEF_WIDGET_FSZ;
        fl_message_window_color_ = DEF_WIN_COLOR_BG;
        fl_message_label_color_ = DEF_WIN_COLOR_FG;
        fl_message_button_color_[ 0 ] = DEF_WIN_COLOR_BG;
        fl_message_button_color_[ 1 ] = DEF_WIN_COLOR_BG;
        fl_message_button_label_color_[ 0 ] = DEF_WIN_COLOR_FG;
        fl_message_button_label_color_[ 1 ] = DEF_WIN_COLOR_FG;
 
        // continue to child components ...
        window->begin();

            int put_x = 5;
            int put_y = 5;
            int put_w = 50;
            int put_h = 20;
            
            put_x += 50;
            
            inpDateYear = new Fl_Input( put_x, put_y, put_w, put_h, "Year:" );
            if ( inpDateYear != NULL )
            {
                APPLY_THEME_INP( inpDateYear );
                inpDateYear->type( FL_INT_INPUT );
                inpDateYear->maximum_size( 4 );
                put_x += put_w + 50;
            }
            
            inpDateMonth = new Fl_Input( put_x, put_y, put_w, put_h, "Month:" );
            if ( inpDateMonth != NULL )
            {
                APPLY_THEME_INP( inpDateMonth );
                inpDateMonth->type( FL_INT_INPUT );
                inpDateMonth->maximum_size( 2 );
                put_x += put_w + 50;
            } 
            
            inpDateDay = new Fl_Input( put_x, put_y, put_w, put_h, "Day:" );
            if ( inpDateDay != NULL )
            {
                APPLY_THEME_INP( inpDateDay );
                inpDateDay->type( FL_INT_INPUT );
                inpDateDay->maximum_size( 2 );
                put_x += put_w + 5;
            } 

            put_w = 180;
            
            btnApplyToday = new Fl_Button( put_x, put_y, put_w, put_h, "Set &now ( date && time )" );
            if ( btnApplyToday != NULL )
            {
                APPLY_THEME_BTN( btnApplyToday );
                btnApplyToday->callback( WidgetCB, this );
            }
            
            put_y += put_h + 5;
            put_w = 50;
            put_x = 5;
            put_x += 50;
            
            inpTimeHour = new Fl_Input( put_x, put_y, put_w, put_h, "Hour:" );
            if ( inpTimeHour != NULL )
            {
                APPLY_THEME_INP( inpTimeHour );
                inpTimeHour->type( FL_INT_INPUT );
                inpTimeHour->maximum_size( 2 );
                put_x += put_w + 50;
            }
            
            inpTimeMinute = new Fl_Input( put_x, put_y, put_w, put_h, "Minute:" );
            if ( inpTimeMinute != NULL )
            {
                APPLY_THEME_INP( inpTimeMinute );
                inpTimeMinute->type( FL_INT_INPUT );
                inpTimeMinute->maximum_size( 2 );
                put_x += put_w + 50;
            } 
            
            put_y += put_h + 5;
            put_w = 130;
            put_x = 5;
            put_x += 50;

            chkShowDateTime = new Fl_Check_Button( put_x, put_y, put_w, put_h, "Show date && time" );
            if ( chkShowDateTime != NULL )
            {
                APPLY_THEME_BTN( chkShowDateTime );
                put_x += put_w + 5;
            }
            
            chkLoopRec = new Fl_Check_Button( put_x, put_y, put_w, put_h, "Do loop record" );
            if ( chkLoopRec != NULL )
            {
                APPLY_THEME_BTN( chkLoopRec );
                put_x += put_w + 5;
            }
            
            chkMotion = new Fl_Check_Button( put_x, put_y, put_w, put_h, "Motion detection" );
            if ( chkMotion != NULL )
            {
                APPLY_THEME_BTN( chkMotion );
                put_x += put_w + 5;
            }
            
            put_y += put_h + 5;
            put_w = 100;
            put_x = 5;
            put_x += 100;
            
            chsResolution = new Fl_Choice( put_x, put_y, put_w, put_h, "Resolution:" );
            if ( chsResolution != NULL )
            {
                APPLY_THEME_CHOICE( chsResolution );
                
                chsResolution->add( "1080p" );
                chsResolution->add( "720p" );
                chsResolution->value( 0 );
            }
            
            put_h = btnApplyToday->h();
            put_w = btnApplyToday->w();
            put_x = btnApplyToday->x();
            put_y = btnApplyToday->y() + btnApplyToday->h() + 5;
            
            btnSave = new Fl_Button( put_x, put_y, put_w, put_h, "Write(&Save) to file" );
            if ( btnSave != NULL )
            {
                APPLY_THEME_BTN( btnSave );
                btnSave->callback( WidgetCB, this );
            }
            
            put_h = 20;
            put_w = window->w() - 10;
            put_x = 5;
            put_y = window->h() - put_h;
            
            boxCopyR = new Fl_Box( put_x, put_y, put_w, put_h, "(C)2021 Raph.K" );
            if ( boxCopyR != NULL )
            {
                APPLY_THEME_BOX( boxCopyR );
                boxCopyR->labelcolor( 0xAA773300 );
                boxCopyR->align( FL_ALIGN_INSIDE | FL_ALIGN_RIGHT );
            }
            
        window->end();
        window->callback( WMain::WidgetCB, this );
 	    window->show();
    }
}

bool WMain::loadtimetxt()
{
    ifstream strm;
    string aline;
    
    strm.open( TIMETXT_FN );
    
    if ( strm.is_open() == true )
    {
        getline( strm, aline );
        strm.close();
    }
    
    unsigned loaded = 0;
    
    if ( aline.size() > 0 )
    {
        // split each datas ...
        vector<string> sepdata = split( aline, ' ' );
        
        if ( sepdata.size() >= 4 )
        {
            // parse date -
            vector<string> dtdata = split( sepdata[0], '-' );
            if ( dtdata.size() >= 3 )
            {
                data_date_year = atoi( dtdata[0].c_str() );
                data_date_month = atoi( dtdata[1].c_str() );
                data_date_day = atoi( dtdata[2].c_str() );
                
                loaded++;
            }
            
            // parse time -
            vector<string> tmdata = split( sepdata[1], ':' );
            if ( tmdata.size() >= 3 )
            {
                data_time_hour = atoi( tmdata[0].c_str() );
                data_time_minute = atoi( tmdata[1].c_str() );
                data_time_second = atoi( tmdata[2].c_str() );
                
                loaded++;
            }
            
            // parse motion
            vector<string> mtdata = split( sepdata[2], ':' );
            if ( mtdata.size() >= 2 )
            {
                if ( mtdata[0] == "motion" )
                {
                    data_motion_v = (unsigned)atoi( mtdata[1].c_str() );
                    loaded++;
                }
            }
            
            // parse parameter .. ( Y/N 0/1 Y/N )
            if ( sepdata[3].size() >= 3 )
            {
                const char* chkstr = sepdata[3].c_str();
                if ( chkstr[0] == 'Y' )
                    data_timedisp_v = 1;
                else
                    data_timedisp_v = 0;
                
                if ( chkstr[1] == '0' )
                    data_res_v = 0; /// 1080p
                else
                    data_res_v = 1; /// 720p
                
                if ( chkstr[2] == 'Y' )
                    data_loop_v = 1;
                else
                    data_loop_v = 0;
                
                loaded++;
            }
            
            if ( loaded > 3 )
            {
                return true;
            }
        }
    }
    
    return false;
}

void WMain::defaultdata( unsigned lvl )
{
    time_t t = time(0);
    tm* now = localtime(&t);

    data_date_year = now->tm_year + 1900;
    data_date_month = now->tm_mon + 1;
    data_date_day = now->tm_mday;
    data_time_hour = now->tm_hour;
    data_time_minute = now->tm_min;
    data_time_second = 0;
    
    if ( lvl == 0 )
    {
        data_motion_v = 0;
        data_timedisp_v = 1;
        data_res_v = 0;
        data_loop_v = 1;
    }
}

void WMain::filldata()
{
    char nmbuff[10] = {0};

    itoa( data_date_year, nmbuff, 10 );
    inpDateYear->value( nmbuff );

    itoa( data_date_month, nmbuff, 10 );
    inpDateMonth->value( nmbuff );
    
    itoa( data_date_day, nmbuff, 10 );
    inpDateDay->value( nmbuff );

    itoa( data_time_hour, nmbuff, 10 );
    inpTimeHour->value( nmbuff );
 
    itoa( data_time_minute, nmbuff, 10 );
    inpTimeMinute->value( nmbuff );

    chkShowDateTime->value( (int)data_timedisp_v );
    chkMotion->value( (int)data_motion_v );
    chkLoopRec->value( (int)data_loop_v );
    
    if ( data_res_v == 0 )
        chsResolution->value( 0 );
    else
        chsResolution->value( 1 );
}

void WMain::apply2ui()
{
    const char* pstr = NULL;
    char nmbuff[10] = {0};
    
    pstr = inpDateYear->value();
    data_date_year = atoi( pstr );
    if ( data_date_year < 2000 )
    {
        data_date_year = 2000;
        itoa( data_date_year, nmbuff, 10 );
        inpDateYear->value( nmbuff );        
    }
    
    pstr = inpDateMonth->value();
    data_date_month = atoi( pstr );
    if ( ( data_date_month < 1 ) || ( data_date_month > 12 ) )
    {
        data_date_month = 1;
        itoa( data_date_month, nmbuff, 10 );
        inpDateMonth->value( nmbuff );
    }
    
    pstr = inpDateDay->value();
    data_date_day = atoi( pstr );
    if ( ( data_date_day < 1 ) || ( data_date_day > 31 ) )
    {
        data_date_day = 1;
        itoa( data_date_day, nmbuff, 10 );
        inpDateDay->value( nmbuff );
    }

    pstr = inpTimeHour->value();
    data_time_hour = atoi( pstr );
    if ( ( data_time_hour < 0 ) || ( data_time_hour > 23 ) )
    {
        data_time_hour = 0;
        itoa( data_time_hour, nmbuff, 10 );
        inpTimeHour->value( nmbuff );
    }
    
    pstr = inpTimeMinute->value();
    data_time_minute = atoi( pstr );
    if ( ( data_time_minute < 0 ) || ( data_time_minute > 59 ) )
    {
        data_time_minute = 0;
        itoa( data_time_minute, nmbuff, 10 );
        inpTimeMinute->value( nmbuff );
    }
 
    data_motion_v = (unsigned)chkMotion->value();
    data_timedisp_v = (unsigned)chkShowDateTime->value();
    data_res_v = (unsigned)chsResolution->value();
    data_loop_v = (unsigned)chkLoopRec->value();
}

bool WMain::writetimetxt()
{
    ofstream strm;
    
    strm.open( TIMETXT_FN );
    
    if ( strm.is_open() == true )
    {
    
        char writedata[80] = {0};
        char cast_d_dt = 'Y';
        char cast_d_lp = 'Y';
        
        if ( data_timedisp_v == 0 )
            cast_d_dt = 'N';
            
        if ( data_loop_v == 0 )
            cast_d_lp = 'N';
        
        snprintf( writedata, 80, "%04u-%02u-%02u %02u:%02u:%02u motion:%u %c%u%c\n",
                  data_date_year,
                  data_date_month,
                  data_date_day,
                  data_time_hour,
                  data_time_minute,
                  0,
                  data_motion_v,
                  cast_d_dt,
                  data_res_v,
                  cast_d_lp );

        strm << writedata;
        strm.close();
        
        return true;
    }
    
    return false;
}
