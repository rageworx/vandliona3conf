# Makefile for FLSCRNSHOT for MinGW-W64 + M-SYS2
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = ar
WRC = windres
FCG = fltk-config --use-images

SRC_PATH = src

# FLTK configs 
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# TARGET settings
TARGET_PKG = vandlionA3conf
TARGET_DIR = ./bin
TARGET_OBJ = ./obj
LIBWIN32S  = -lole32 -luuid -lcomctl32 -lwsock32 -lm -lgdi32 
LIBWIN32S += -luser32 -lkernel32 -lShlwapi -lcomdlg32 -lwinmm

# DEFINITIONS
DEFS += -D_POSIX_THREADS -DSUPPORT_DRAGDROP -DMININI_ANSI
DEFS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE 
DEFS += -D_THREAD_SAFE -D_REENTRANT

# Compiler optiops 
COPTS +=

# CC FLAGS
CFLAGS += -Ires
CFLAGS += -Isrc
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# Windows Resource Flags
WFLGAS  = $(CFLAGS)

# LINK FLAG
LFLAGS += -L$(FIMG_PATH)
LFLAGS += -L$(LOCL_PATH)
LFLAGS += -static
LFLAGS += ${FLTKCFG_LFG}
LFLAGS += $(LIBWIN32S)
LFLAGS += -mms-bitfields -mwindows -fomit-frame-pointer
LFLAGS += -ffast-math -fexceptions -fopenmp -O3 -s

# Sources
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Windows resource
WRES = res/resource.rc

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
WROBJ = $(TARGET_OBJ)/resource.o

.PHONY: prepare clean

all: prepare continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(WROBJ): $(WRES) res/resource.h
	@echo "Building windows resource ..."
	@$(WRC) -i $(WRES) $(WFLAGS) -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(WROBJ)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
