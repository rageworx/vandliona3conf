## A fast and simple Vandlion or Boblov A3 time.txt  configurator

(C)2020, 2021 Raphael Kim

### Requirements

- MSYS2
- MinGW-W64
- FLTK-1.3.5-2-ts

### How to build ?

- just type `make` to build source on MSYS2.

### Download prebuilt widows binaries

- https://bitbucket.org/rageworx/vandliona3conf/downloads/

### How to use ?

- Just download zip and extract it or build source to make 'vandlionA3conf.exe'.
- Copy 'vandlionA3conf.exe' into your device SD, place same directory ( maybe root of SD ) with 'time.txt'.
- Just run 'vandlionA3conf.exe' to configure time.txt.